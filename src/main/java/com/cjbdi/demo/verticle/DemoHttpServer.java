package com.cjbdi.demo.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;

import io.vertx.ext.auth.jwt.JWTAuth;

import io.vertx.ext.auth.jwt.impl.JWTUser;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import io.vertx.ext.web.sstore.SessionStore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cjbdi.demo.config.AppConfiguration;
import com.cjbdi.demo.module.product.ProductResource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class DemoHttpServer extends AbstractVerticle {

	static Logger logger = LoggerFactory.getLogger(DemoHttpServer.class);
	@Autowired
	AppConfiguration configuration;

	@Autowired
	JWTAuth jwtAuth;
	
	@Autowired
	ProductResource productResource;

	@Override
	public void start() throws Exception {

		Router mainRouter = Router.router(vertx);

		SessionStore store = LocalSessionStore.create(vertx, "myapp.sessionmap");
		SessionHandler sessionHandler = SessionHandler.create(store);
		mainRouter.route().handler(sessionHandler);

		mainRouter.route().handler(BodyHandler.create());
		
		mainRouter.route().handler(context -> {
			String normalisedPath = context.normalisedPath();

			logger.debug("client request to path:" + normalisedPath);

			// 首页（前端首页），Visitor可访问，无需验证 token
			if (normalisedPath.startsWith("/auth/login")) {
				context.next();
			} else {
				JWTAuthHandler.create(jwtAuth).handle(context);
			}

		}).failureHandler(routingContext->{
			Throwable t=routingContext.failure();
			//routingContext.response().setStatusCode(400);
			routingContext.response().end(new JsonObject().put("code", routingContext.statusCode()).put("cause", t.getMessage()).toString());
			
		});
		Route staticRoute = mainRouter.route().path("/static/*");
		// Serve the static pages
		staticRoute.handler(StaticHandler.create());

		// 处理 login
		Route loginRoute = mainRouter.route().path("/auth/login/*");
		loginRoute.handler(routingContext -> {
			String name = routingContext.request().getParam("name");
			logger.debug("username:" + name);
			String secret = routingContext.request().getParam("secret");
			
			String token = jwtAuth.generateToken(new JsonObject().put("name", name));
			Session session = routingContext.session();
			session.put("token", token);
			routingContext.response().setStatusCode(200).end(token);
		});

		// 处理 login
		Route infoRoute = mainRouter.route().path("/auth/info");
		infoRoute.handler(routingContext -> {
			JWTUser jwtUser = (JWTUser) routingContext.user();
			JsonObject jsonObject = jwtUser.principal();
			logger.debug(jsonObject.toString());
			Session session = routingContext.session();
			String token = session.get("token");
			routingContext.response().setStatusCode(200).end(token);
		});

		productResource.mount(mainRouter);
		
		vertx.createHttpServer().requestHandler(mainRouter).listen(configuration.getHttpPort());
		logger.info("vertx start a http server on port:" + configuration.getHttpPort());
	}
}
