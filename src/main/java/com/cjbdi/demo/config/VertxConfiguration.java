package com.cjbdi.demo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cjbdi.demo.module.product.ProductResource;
import com.cjbdi.demo.verticle.DemoHttpServer;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLClient;

@Configuration
public class VertxConfiguration {

	static Logger logger = LoggerFactory.getLogger(VertxConfiguration.class);
	
	@Value("${vertx.jwt.algorithm}")
	String algorithm;
	
	@Value("${vertx.jwt.public_key}")
	String public_key;
	

	
	@Value("${database.url}")
	String database_url;
	
	@Value("${database.driver_class}")
	String databse_driver_class;
	
	@Value("${database.max_pool_size}")
	int database_max_pool_size;
	
	@Value("${database.user}")
	String database_user;
	
	@Value("${database.password}")
	String database_password;
	
	@Bean
	public Vertx vertx() {
		Vertx vertx = Vertx.vertx(new VertxOptions());
		return vertx;
	}
	
	@Bean
	public JWTAuth jwtAuth(Vertx vertx) {
		JWTAuthOptions jwtAuthOptions = new JWTAuthOptions();
		jwtAuthOptions.addPubSecKey(
				new PubSecKeyOptions().setAlgorithm(algorithm).setPublicKey(public_key).setSymmetric(true));
		JWTAuth jwtAuth = JWTAuth.create(vertx, jwtAuthOptions);
		return jwtAuth;
	}
	@Bean
	public SQLClient  sqlClient (Vertx vertx)
	{
		SQLClient  client = JDBCClient.createShared(vertx, new JsonObject()
		        .put("url", database_url)
		        .put("driver_class", databse_driver_class)
		        .put("max_pool_size", database_max_pool_size)
		        .put("user", database_user)
		        .put("password", database_password));
		
		return client;
	}
	@Bean
	public ProductResource productResource(Vertx vertx) {
		
		ProductResource productResource=new ProductResource(vertx,"/product");
		return productResource;
	}
	
	
}
