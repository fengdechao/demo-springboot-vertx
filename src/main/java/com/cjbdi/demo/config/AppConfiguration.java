package com.cjbdi.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

@Configuration
public class AppConfiguration {

	@Value("${vertx.http.port}")
	int httpPort;

	public int getHttpPort() {
		return httpPort;
	}

	public void setHttpPort(int httpPort) {
		this.httpPort = httpPort;
	}

	int httpPort() {
		return httpPort;
	}
	
	

}
