package com.cjbdi.demo.module.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class ProductResource {

	@Autowired
	SQLClient sqlClient;

	Router productRouter;
	String path = "/product";

	public ProductResource(Vertx vertx, String path) {
		productRouter = Router.router(vertx);
		this.path = path;
	}

	public void mount(Router router) {

		productRouter.get("/").handler(this::productList);
		productRouter.get("/test").handler(this::test);
		router.mountSubRouter(path, productRouter);
	}

	private void productList(RoutingContext context) {
		HttpServerRequest request = context.request();
		HttpServerResponse response = context.response();
		response.putHeader("content-type", "application/json; charset=utf-8");
		response.end("product list");
	}

	private void test(RoutingContext context) {
		String sql = "select count(*) from subscription";
		sqlClient.getConnection(conn -> {
			if (conn.failed()) {
				System.err.println(" fail:" + conn.cause().getMessage());
				return;
			} else {
				SQLConnection connection = conn.result();
				connection.query(sql, res -> {
					if (res.failed()) {
						throw new RuntimeException(res.cause());
					} else {
						ResultSet rs = res.result();
						for(int i=0;i<rs.getNumColumns();i++) {
							System.out.println(rs.getColumnNames().get(i));
						}
						System.out.println(rs.getNumRows());
						List<JsonArray> jsonArrayList = rs.getResults();
						context.response().end(jsonArrayList.get(0).toString());
					}
				});
				
				Handler<AsyncResult<ResultSet>> rh=ar->{
					List<JsonObject> lists=ar.result().getRows();
					for(int i=0;i<ar.result().getNumColumns();i++) {
						System.out.println(ar.result().getColumnNames().get(i));
					}
					System.out.println(lists.get(0).toString());
				};
				
				query(connection,new JsonArray().add(6),rh);

			}

		});
		
		
		
		
	}

	private void query(SQLConnection conn, JsonArray params, Handler<AsyncResult<ResultSet>> resultHandler) {
		conn.queryWithParams("select * from subscription where subscription_id=?", params,resultHandler);

			// and close the connection
			conn.close(clc -> {
				if (clc.failed()) {
					throw new RuntimeException(clc.cause());
				}
			});
		
	}
}
