package com.cjbdi.demo;


import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ApplicationContext;

import com.cjbdi.demo.verticle.DemoHttpServer;

import io.vertx.core.Vertx;

@SpringBootApplication
public class MainApplication {
	static Logger logger=LoggerFactory.getLogger(MainApplication.class);

	@Autowired 
	DemoHttpServer demoHttpServer;
	
	@Autowired
	Vertx vertx;
	
	public static void main(String[] args) {
		//boot 运行
		SpringApplication sa = new SpringApplication(MainApplication.class);
		sa.addListeners(new ApplicationPidFileWriter());
		ApplicationContext context = sa.run(args);
		
		logger.info(context.getApplicationName()+" started !");
	}
	
	 @PostConstruct
	  public void deployVerticle() {
	    vertx.deployVerticle(demoHttpServer);
	  }

}
